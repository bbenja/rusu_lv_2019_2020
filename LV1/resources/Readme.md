Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. 
Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). 
Međutim, skripta ima bugove i ne radi kako je zamišljeno.

Promjene 1:
ispravljene greške
- sintaksa: korišten input umjesto raw_input, dodane zagrade za input i print
- logika: dodano prebacivanje riječi u lowercase i brojanje riječi koje se ponavljaju