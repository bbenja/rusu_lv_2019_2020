import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')
#print(len(mtcars))
#print(mtcars)

cars_4_cyl = mtcars[mtcars.cyl == 4]
cars_6_cyl = mtcars[mtcars.cyl == 6]
cars_8_cyl = mtcars[mtcars.cyl == 8]
automatic = mtcars[mtcars.am == 1]
manual = mtcars[mtcars.am == 0]
"""
plt.figure(1)
ax = plt.subplot(111)
ax.bar(x=cars_4_cyl.car, height=cars_4_cyl.mpg, color='b', label='4 cyl')
ax.bar(x=cars_6_cyl.car, height=cars_6_cyl.mpg, color='g', label='6 cyl')
ax.bar(x=cars_8_cyl.car, height=cars_8_cyl.mpg, color='r', label='8 cyl')
ax.set_xlabel('Cylinders')
ax.set_ylabel('MPG')
ax.set_xticks("None")
ax.legend()
ax.autoscale(tight=True)
plt.show()
"""
"""
plt.figure(2)
ax = plt.subplot(111)
ax.boxplot(x=cars_4_cyl.wt)
ax.boxplot(x=cars_6_cyl.wt)
ax.boxplot(x=cars_8_cyl.wt)
ax.autoscale(tight=True)
plt.show()
"""
"""
plt.figure(3)
ax = plt.subplot(111)
ax.bar(x=manual.car, height=manual.mpg, label='manual')
ax.bar(x=automatic.car, height=automatic.mpg, label='automatic')
ax.autoscale(tight=True)
plt.axhline(y=manual.mpg.mean(), color='cyan', label="manual AVG")
plt.axhline(y=automatic.mpg.mean(), color='brown', label="automatic AVG")
ax.set_xticks("None")
ax.set_xlabel('Transmission')
ax.set_ylabel('MPG')
ax.legend()
plt.show()
"""
ax = plt.subplot(111)
manual = manual.sort_values(by='hp')
automatic = automatic.sort_values(by='hp')
plt.plot(manual.hp, manual.qsec, color='blue', label="manual")
plt.plot(automatic.hp, automatic.qsec, color='orange', label="automatic")
plt.axhline(y=manual.qsec.mean(), color='cyan', label="manual AVG")
plt.axhline(y=automatic.qsec.mean(), color='brown', label="automatic AVG")
ax.set_xlabel('HP')
ax.set_ylabel('QSEC')
ax.legend()
plt.show()