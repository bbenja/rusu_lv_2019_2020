import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
mtcars = pd.read_csv('../resources/mtcars.csv')
#print(len(mtcars))
#print(mtcars)

#5 s najvecom potrosnjom (sort) ============================
worst_mpg = mtcars.sort_values(by='mpg')
#print(worst_mpg.head(5)[['car', 'mpg']])

#3 automobila s 8 cilindara s najmanjom potrosnjom==========
best_mpg_8_cyl = mtcars[mtcars.cyl == 8].sort_values(by='mpg', ascending=False)
#print(best_mpg_8_cyl.head(3)[['car', 'mpg', 'cyl']])

#srednja potrošnja s 6 cilindara============================
avg_6_cyl = mtcars[mtcars.cyl == 6]
#print(avg_6_cyl.mpg.mean())

#srednja potrošnja s 6 cilindara izmedju 2000 i 2200 lbs====
avg_4_cyl_2000lbs = mtcars[(mtcars.cyl == 4) & (mtcars.wt >= 2.0) & (mtcars.wt <= 2.2)]
#print(avg_4_cyl_2000lbs.mpg.mean())

#broj auta s rucnim i automatskim mjenjacem=================
am = mtcars[mtcars.am == 0]
#print(len(am))
#print(len(mtcars) - len(am))

#broj automatika preko 100hp================================
am_100hp = am[am.hp > 100]
#print(len(am_100hp))

#masa svakog automobila u kg================================
mtcars.wt *= 0.45359237 * 1000
print(mtcars.wt)