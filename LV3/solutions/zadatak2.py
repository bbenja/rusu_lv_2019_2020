import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')
#print(mtcars)

cars_4_cyl = mtcars[mtcars.cyl == 4]
cars_6_cyl = mtcars[mtcars.cyl == 6]
cars_8_cyl = mtcars[mtcars.cyl == 8]
automatic = mtcars[mtcars.am == 1]
manual = mtcars[mtcars.am == 0]

cyl_mpg = mtcars.groupby('cyl').mean()
#print(cyl_mpg)
plt.bar([4, 6, 8], cyl_mpg['mpg'], label ="AVG MPG")
plt.xlabel('Cylinders')
plt.ylabel('MPG')
plt.legend()
plt.autoscale()
plt.show()

cyl_wt = mtcars[['cyl', 'wt']]
cyl_wt.boxplot(by='cyl')
plt.autoscale(tight=True)
plt.xlabel('Cylinders')
plt.ylabel('Weight')
plt.show()

plt.figure(3)
ax = plt.subplot(111)
ax.bar(x=manual.car, height=manual.mpg, label='manual')
ax.bar(x=automatic.car, height=automatic.mpg, label='automatic')
ax.autoscale(tight=True)
plt.axhline(y=manual.mpg.mean(), color='cyan', label="manual AVG")
plt.axhline(y=automatic.mpg.mean(), color='brown', label="automatic AVG")
ax.set_xticks("None")
ax.set_xlabel('Transmission')
ax.set_ylabel('MPG')
ax.legend()
plt.show()

ax = plt.subplot(111)
manual = manual.sort_values(by='hp')
automatic = automatic.sort_values(by='hp')
plt.scatter(manual.hp, manual.qsec, color='blue')
plt.scatter(automatic.hp, automatic.qsec, color='orange')
plt.plot(manual.hp, manual.qsec, color='blue', label="manual")
plt.plot(automatic.hp, automatic.qsec, color='orange', label="automatic")
plt.axhline(y=manual.qsec.mean(), color='cyan', label="manual AVG")
plt.axhline(y=automatic.qsec.mean(), color='brown', label="automatic AVG")
ax.set_xlabel('HP')
ax.set_ylabel('QSEC')
ax.legend()
plt.show()
