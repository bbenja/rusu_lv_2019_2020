import urllib
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import array

# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=4&vrijemeOd=02.01.2017&vrijemeDo=31.12.2017'
airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)
df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
 try:
  obj = list(list(root)[i])
 except:
  break

row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
row_s = pd.Series(row)
row_s.name = i
df = df.append(row_s)
df.mjerenje[i] = float(df.mjerenje[i])
i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)

df.plot(y='mjerenje', x='vrijeme')

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek

max_pm10 = df[['vrijeme', 'mjerenje']].sort_values(by='mjerenje', ascending=False)
print(max_pm10.head(3))

dates = []
flag = 0
lastday = df.vrijeme.dt.dayofyear.max()
for i in range(1, lastday):
    flag = 0
    for day in df.vrijeme.dt.dayofyear:
        if(day == i):
            flag = 1
    if(flag == 0):
        #print(i/30)
        dates.append(i.__round__())

months = array.array(0, 12)
for i in dates:
    for index in months:
        if(months[index] == dates/30):
            months[index]+=1
print(months)
