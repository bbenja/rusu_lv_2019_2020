import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
import sklearn.metrics as metrics
from sklearn.preprocessing import PolynomialFeatures

def generate_data(n):
    # prva klasa
    n1 = int(n / 2)
    x1_1 = np.random.normal(0.0, 2, (n1, 1));
    # x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1, 2) + np.random.standard_normal((n1, 1));
    y_1 = np.zeros([n1, 1])
    temp1 = np.concatenate((x1_1, x2_1, y_1), axis=1)

    # druga klasa
    n2 = int(n - n / 2)
    x_2 = np.random.multivariate_normal((0, 10), [[0.8, 0], [0, 1.2]], n2);
    y_2 = np.ones([n2, 1])
    temp2 = np.concatenate((x_2, y_2), axis=1)

    data = np.concatenate((temp1, temp2), axis=0)

    # permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices, :]

    return data


def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j) / float(a))
        norm_conf.append(tmp_arr)

    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')

    width = len(c_matrix)
    height = len(c_matrix[0])

    for x in range(width):
        for y in range(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color='green', size=20)

    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])

    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()


np.random.seed(242)
data_train = generate_data(200)
np.random.seed(12)
set_test = generate_data(100)

x_test = set_test[:,0:2]
x1_test = set_test[:,0]
x2_test = set_test[:,1]
y_test = set_test[:,2]
x1_train = data_train[:, 0]
x2_train = data_train[:, 1]
x_train = data_train[:, 0:2]
y_train = data_train[:,2]

logistic = lm.LogisticRegression()
logistic.fit(x_train, y_train)
logistic.predict(x_train)
th1 = logistic.intercept_
th2 = logistic.coef_

y = logistic.predict(x_test)
y_color = []
for i in range(0,100):
    if y[i] == y_test[i]:
        y_color.append('green')
    else:
        y_color.append('black')

confusion_matrix = metrics.confusion_matrix(y_test, y)
print(confusion_matrix)

TP = confusion_matrix[0][0]
FP = confusion_matrix[0][1]
FN = confusion_matrix[1][0]
TN = confusion_matrix[1][1]

accuracy = (TP + TN) / (TP + TN + FP + FN)
misclassification_rate = 1 - accuracy
precision = TP / (TP + FP)
recall = TP / (TP + FN)
specificity = TN / (TN + FP)

print("Accuracy: " +str(accuracy))
print("Misclass. rate: " + str(misclassification_rate))
print("Precision: " + str(precision))
print("Recall: " + str(recall))
print("Specificity: " + str(specificity))

plot_confusion_matrix(confusion_matrix)

