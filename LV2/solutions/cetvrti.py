{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "np.random.seed(55)\n",
    "dieRolls = np.random.randint(low = 1, high = 7, size = 100)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(array([19.,  0.,  9.,  0., 18.,  0., 17.,  0., 11.,  0., 26.]),\n",
       " array([1.        , 1.45454545, 1.90909091, 2.36363636, 2.81818182,\n",
       "        3.27272727, 3.72727273, 4.18181818, 4.63636364, 5.09090909,\n",
       "        5.54545455, 6.        ]),\n",
       " <a list of 11 Patch objects>)"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAXAAAAD4CAYAAAD1jb0+AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4yLjIsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+WH4yJAAALxElEQVR4nO3dbYhlBR3H8d9P16h8QGWvsqg0JSJJ0CrDViyIZcr6QCoUJCQSxvpCQ0mIzTfau32R2psQVndzIx8QH1BSTDHDhLJmbdO1URTZanVzRiTU3oj668WcpWGc8d6599x757/z/cBw7z333Hv+54Vfz545946TCABQzyHjHgAA0B8CDgBFEXAAKIqAA0BRBBwAilozyo2tXbs2ExMTo9wkAJS3a9eut5J0Fi4facAnJiY0NTU1yk0CQHm2/7HYck6hAEBRBBwAiiLgAFAUAQeAogg4ABRFwAGgKAIOAEURcAAoioADQFEj/SQmAKwUE1seGen29m69oPX35AgcAIoi4ABQFAEHgKIIOAAU1TXgtk+y/ZTtadsv2r6mWX6j7ddt725+zh/+uACAA3q5CuUDSdclec72kZJ22X6iee6WJD8b3ngAgKV0DXiS/ZL2N/fftT0t6YRhDwYA+GTLOgdue0LS6ZKebRZdbft52ztsH7PEazbbnrI9NTs7O9CwAID/6zngto+QdL+ka5O8I+lWSSdLWq+5I/SbFntdkm1JJpNMdjof+5NuAIA+9RRw24dpLt53JnlAkpK8meTDJB9Juk3ShuGNCQBYqJerUCxpu6TpJDfPW75u3mqXSNrT/ngAgKX0chXKRkmXSXrB9u5m2fWSLrW9XlIk7ZV05VAmBAAsqperUJ6R5EWeerT9cQAAveKTmABQFAEHgKIIOAAURcABoCgCDgBFEXAAKIqAA0BRBBwAiiLgAFAUAQeAogg4ABRFwAGgKAIOAEURcAAoioADQFEEHACKIuAAUBQBB4CiCDgAFEXAAaAoAg4ARRFwACiKgANAUQQcAIoi4ABQFAEHgKIIOAAURcABoCgCDgBFEXAAKKprwG2fZPsp29O2X7R9TbP8WNtP2H6luT1m+OMCAA7o5Qj8A0nXJfmipK9Kusr2aZK2SHoyySmSnmweAwBGpGvAk+xP8lxz/11J05JOkHSRpJ3NajslXTysIQEAH7esc+C2JySdLulZSccn2S/NRV7ScW0PBwBYWs8Bt32EpPslXZvknWW8brPtKdtTs7Oz/cwIAFhETwG3fZjm4n1nkgeaxW/aXtc8v07SzGKvTbItyWSSyU6n08bMAAD1dhWKJW2XNJ3k5nlPPSzp8ub+5ZIean88AMBS1vSwzkZJl0l6wfbuZtn1krZKutf2FZL+Kek7wxkRALCYrgFP8owkL/H02e2OAwDoFZ/EBICiCDgAFNXLOfAVYWLLIyPd3t6tF4x0ewCwXByBA0BRBBwAiiLgAFAUAQeAogg4ABRFwAGgKAIOAEWVuQ4cdXENPzAcHIEDQFEEHACKIuAAUBQBB4CiCDgAFEXAAaAoAg4ARXEdODAArnHHOHEEDgBFEXAAKIqAA0BRBBwAiiLgAFAUAQeAogg4ABRFwAGgKAIOAEURcAAoioADQFEEHACK6hpw2ztsz9jeM2/ZjbZft727+Tl/uGMCABbq5Qj8DkmbFll+S5L1zc+j7Y4FAOima8CTPC3p7RHMAgBYhkHOgV9t+/nmFMsxS61ke7PtKdtTs7OzA2wOADBfvwG/VdLJktZL2i/ppqVWTLItyWSSyU6n0+fmAAAL9RXwJG8m+TDJR5Juk7Sh3bEAAN30FXDb6+Y9vETSnqXWBQAMR9e/iWn7bklnSVpre5+kGySdZXu9pEjaK+nKIc4IAFhE14AnuXSRxduHMAsAYBn4JCYAFEXAAaAoAg4ARRFwACiKgANAUQQcAIoi4ABQFAEHgKIIOAAURcABoCgCDgBFEXAAKIqAA0BRBBwAiiLgAFAUAQeAogg4ABRFwAGgKAIOAEURcAAoioADQFEEHACKIuAAUBQBB4CiCDgAFEXAAaCoNeMeAMDKNbHlkZFta+/WC0a2rYMFR+AAUBQBB4CiCDgAFEXAAaCorgG3vcP2jO0985Yda/sJ2680t8cMd0wAwEK9HIHfIWnTgmVbJD2Z5BRJTzaPAQAj1DXgSZ6W9PaCxRdJ2tnc3ynp4pbnAgB00e858OOT7Jek5va4pVa0vdn2lO2p2dnZPjcHAFho6L/ETLItyWSSyU6nM+zNAcCq0W/A37S9TpKa25n2RgIA9KLfgD8s6fLm/uWSHmpnHABAr3q5jPBuSX+UdKrtfbavkLRV0jm2X5F0TvMYADBCXb/MKsmlSzx1dsuzAACWgU9iAkBRBBwAiuL7wFcIvncZwHJxBA4ARRFwACiKgANAUQQcAIoi4ABQFAEHgKIIOAAURcABoCgCDgBFEXAAKIqAA0BRBBwAiiLgAFAUAQeAogg4ABRFwAGgKAIOAEURcAAoioADQFEEHACKIuAAUBQBB4CiCDgAFEXAAaAoAg4ARRFwACiKgANAUQQcAIpaM8iLbe+V9K6kDyV9kGSyjaEAAN0NFPDG15O81cL7AACWgVMoAFDUoAGPpMdt77K9ebEVbG+2PWV7anZ2dsDNAQAOGDTgG5OcIek8SVfZPnPhCkm2JZlMMtnpdAbcHADggIECnuSN5nZG0oOSNrQxFACgu74Dbvtw20ceuC/pXEl72hoMAPDJBrkK5XhJD9o+8D53JXmslakAAF31HfAkr0n6couzAACWgcsIAaAoAg4ARRFwACiKgANAUQQcAIoi4ABQFAEHgKIIOAAURcABoCgCDgBFEXAAKIqAA0BRBBwAiiLgAFAUAQeAogg4ABRFwAGgKAIOAEURcAAoioADQFEEHACKIuAAUBQBB4CiCDgAFEXAAaAoAg4ARRFwACiKgANAUQQcAIoi4ABQFAEHgKIGCrjtTbZftv2q7S1tDQUA6K7vgNs+VNIvJJ0n6TRJl9o+ra3BAACfbJAj8A2SXk3yWpL3Jd0j6aJ2xgIAdOMk/b3Q/rakTUl+0Dy+TNJXkly9YL3NkjY3D0+V9HKfs66V9Fafr62KfV4d2OfVYZB9/lySzsKFawYYxoss+9j/DZJsk7RtgO3MbcyeSjI56PtUwj6vDuzz6jCMfR7kFMo+SSfNe3yipDcGGwcA0KtBAv4XSafY/rztT0n6rqSH2xkLANBN36dQknxg+2pJv5V0qKQdSV5sbbKPG/g0TEHs8+rAPq8Ore9z37/EBACMF5/EBICiCDgAFLXiA257h+0Z23vGPcuo2D7J9lO2p22/aPuacc80TLY/bfvPtv/W7O9Pxz3TqNg+1PZfbf9m3LOMgu29tl+wvdv21LjnGQXbR9u+z/ZLzX/TX2vtvVf6OXDbZ0p6T9Kvknxp3POMgu11ktYlec72kZJ2Sbo4yd/HPNpQ2Lakw5O8Z/swSc9IuibJn8Y82tDZ/pGkSUlHJblw3PMMm+29kiaTrJoP8djeKekPSW5vrtj7bJL/tPHeK/4IPMnTkt4e9xyjlGR/kuea++9KmpZ0wninGp7Mea95eFjzs7KPLFpg+0RJF0i6fdyzYDhsHyXpTEnbJSnJ+23FWyoQ8NXO9oSk0yU9O95Jhqs5lbBb0oykJ5Ic1Pvb+LmkH0v6aNyDjFAkPW57V/M1Gwe7L0ialfTL5lTZ7bYPb+vNCfgKZvsISfdLujbJO+OeZ5iSfJhkveY+0bvB9kF9usz2hZJmkuwa9ywjtjHJGZr7FtOrmlOkB7M1ks6QdGuS0yX9V1JrX71NwFeo5lzw/ZLuTPLAuOcZleafl7+XtGnMowzbRknfas4J3yPpG7Z/Pd6Rhi/JG83tjKQHNfetpgezfZL2zfsX5X2aC3orCPgK1PxSb7uk6SQ3j3ueYbPdsX10c/8zkr4p6aXxTjVcSX6S5MQkE5r7GorfJfnemMcaKtuHN7+UV3Ma4VxJB/XVZUn+Lelftk9tFp0tqbWLEQb5NsKRsH23pLMkrbW9T9INSbaPd6qh2yjpMkkvNOeFJen6JI+OcaZhWidpZ/NHQg6RdG+SVXFZ3SpzvKQH545PtEbSXUkeG+9II/FDSXc2V6C8Jun7bb3xir+MEACwOE6hAEBRBBwAiiLgAFAUAQeAogg4ABRFwAGgKAIOAEX9D65jrJjy0GVBAAAAAElFTkSuQmCC\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {
      "needs_background": "light"
     },
     "output_type": "display_data"
    }
   ],
   "source": [
    "plt.hist(dieRolls, bins = 11)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
