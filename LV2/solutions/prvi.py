{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "import re"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "file = open(\"../resources/mbox-short.txt\")\n",
    "strings = file.read()\n",
    "mails = re.findall('[a-zA-Z0-9-+_.]+@[a-zA-Z0-9-]+\\\\.[a-zA-Z0-9-.]+', strings)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['stephen.marquard', 'postmaster', '200801051412.m05ECIaH010327', 'source', 'louis', '200801042308.m04N8v6O008125', 'zqian', '200801042109.m04L92hb007923', 'rjlowe', '200801042044.m04Kiem3007881', '200801042001.m04K1cO0007738', '200801041948.m04JmdwO007705', 'cwen', '200801041635.m04GZQGZ007313', 'hu2', '200801041633.m04GX6eG007292', 'gsilver', '200801041611.m04GB1Lb007221', '200801041610.m04GA5KP007209', '200801041609.m04G9EuX007197', '200801041608.m04G8d7w007184', 'wagnermr', '200801041537.m04Fb6Ci007092', '200801041515.m04FFv42007050', 'antranig', '200801041502.m04F21Jo007031', 'gopal.ramasammycook', '200801041403.m04E3psW006926', 'david.horwitz', '200801041200.m04C0gfK006793', '200801041106.m04B6lK3006677', '200801040947.m049lUxo006517', 'josrodri', '200801040932.m049W2i5006493', '200801040905.m0495rWB006420', '200801040023.m040NpCc005473', '200801032216.m03MGhDa005292', 'ray', '200801032205.m03M5Ea7005273', '200801032133.m03LX3gG005191', '200801032127.m03LRUqH005177', '200801032122.m03LMFo4005148']\n"
     ]
    }
   ],
   "source": [
    "usernames = re.findall('\\S+@\\S+', str(mails))\n",
    "usernames = []\n",
    "for m in mails:\n",
    "    usernames.append(m.split(\"@\")[0])\n",
    "usernames = list(dict.fromkeys(usernames))\n",
    "print(usernames)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
