{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [],
   "source": [
    "import re"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "file = open(\"../resources/mbox-short.txt\")\n",
    "strings = file.read()\n",
    "mails = re.findall('[a-zA-Z0-9-+_.]+@[a-zA-Z0-9-]+\\\\.[a-zA-Z0-9-.]+', strings)\n",
    "\n",
    "usernames = re.findall('\\S+@\\S+', str(mails))\n",
    "usernames = []\n",
    "for m in mails:\n",
    "    usernames.append(m.split(\"@\")[0])\n",
    "usernames = list(dict.fromkeys(usernames))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['stephen.marquard', 'postmaster', '200801051412.m05ECIaH010327', 'zqian', 'wagnermr', 'antranig', 'gopal.ramasammycook', 'david.horwitz', '200801032216.m03MGhDa005292', 'ray', '200801032205.m03M5Ea7005273']\n",
      "['postmaster', 'zqian', 'wagnermr', 'ray']\n",
      "['source', 'louis', '200801042308.m04N8v6O008125', '200801042109.m04L92hb007923', 'rjlowe', '200801042044.m04Kiem3007881', '200801042001.m04K1cO0007738', '200801041948.m04JmdwO007705', 'cwen', '200801041635.m04GZQGZ007313', 'hu2', '200801041633.m04GX6eG007292', 'gsilver', '200801041611.m04GB1Lb007221', '200801041610.m04GA5KP007209', '200801041609.m04G9EuX007197', '200801041608.m04G8d7w007184', '200801041537.m04Fb6Ci007092', '200801041515.m04FFv42007050', '200801041502.m04F21Jo007031', '200801041403.m04E3psW006926', '200801041200.m04C0gfK006793', '200801041106.m04B6lK3006677', '200801040947.m049lUxo006517', 'josrodri', '200801040932.m049W2i5006493', '200801040905.m0495rWB006420', '200801040023.m040NpCc005473', '200801032133.m03LX3gG005191', '200801032127.m03LRUqH005177', '200801032122.m03LMFo4005148']\n",
      "['200801051412.m05ECIaH010327', '200801042308.m04N8v6O008125', '200801042109.m04L92hb007923', '200801042044.m04Kiem3007881', '200801042001.m04K1cO0007738', '200801041948.m04JmdwO007705', '200801041635.m04GZQGZ007313', 'hu2', '200801041633.m04GX6eG007292', '200801041611.m04GB1Lb007221', '200801041610.m04GA5KP007209', '200801041609.m04G9EuX007197', '200801041608.m04G8d7w007184', '200801041537.m04Fb6Ci007092', '200801041515.m04FFv42007050', '200801041502.m04F21Jo007031', '200801041403.m04E3psW006926', '200801041200.m04C0gfK006793', '200801041106.m04B6lK3006677', '200801040947.m049lUxo006517', '200801040932.m049W2i5006493', '200801040905.m0495rWB006420', '200801040023.m040NpCc005473', '200801032216.m03MGhDa005292', '200801032205.m03M5Ea7005273', '200801032133.m03LX3gG005191', '200801032127.m03LRUqH005177', '200801032122.m03LMFo4005148']\n",
      "['stephen.marquard', 'postmaster', 'source', 'louis', 'zqian', 'rjlowe', 'cwen', 'gsilver', 'wagnermr', 'antranig', 'gopal.ramasammycook', 'david.horwitz', 'josrodri', 'ray']\n"
     ]
    }
   ],
   "source": [
    "list1 = []\n",
    "list2 = []\n",
    "list3 = []\n",
    "list4 = []\n",
    "list5 = []\n",
    "for u in usernames:\n",
    "    if(re.findall(r'[a]+', u)):\n",
    "        list1.append(u)\n",
    "    if(re.findall(r'^[b-zA-Z0-9]*[a][b-zA-Z0-9]*$', u)):\n",
    "        list2.append(u)\n",
    "    if(re.findall(r'^[b-zA-Z0-9]*[^a][b-zA-Z0-9]*$', u)):\n",
    "        list3.append(u)\n",
    "    if(re.findall(r'[0-9]+', u)):\n",
    "        list4.append(u)\n",
    "    if(re.findall(r'^[a-z]*[^0-9][a-z]*$', u)):\n",
    "        list5.append(u)\n",
    "\n",
    "print(list1)\n",
    "print(list2)\n",
    "print(list3)\n",
    "print(list4)\n",
    "print(list5)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
